#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include <assert.h>

//#include <stdio.h>

#define PAGE_SIZE 4096
void test1(){
  heap_init(20);
  int32_t* data = _malloc(16);
  assert(data);
  assert(data[2] == 0); // data can be accessed
  heap_term();
  printf("Test #1 passed: default successful allocation\n");
}
void test2(){
  heap_init(200);
  void* data1 = _malloc(16);
  void* data2 = _malloc(100);
  void* data3 = _malloc(10);

  assert(data1);
  assert(data2);
  assert(data3);
  _free(data2);
  assert(_malloc(100) == data2);
  heap_term();
  printf("Test #2 passed: one block freed\n");
}
void test3(){
  heap_init(200);
  void* data1 = _malloc(16);
  void* data2 = _malloc(100);
  void* data3 = _malloc(10);

  assert(data1);
  assert(data2);
  assert(data3);
  _free(data2);
  _free(data1);
  assert(_malloc(100) < data3);
  heap_term();
  printf("Test #3 passed: two blocks freed\n");
}
bool on_different_pages(void* a, void *b){
  return (uintptr_t)a / PAGE_SIZE != (uintptr_t)b / PAGE_SIZE;
}
void test4(){
  heap_init(20);
  void* data1 = _malloc(100);
  assert(data1);
  printf("Test #4 passed: heap enlarged\n");
}
void test5(){
  heap_init(0);
  void* data1 = _malloc(100);
  assert(data1);
  void * some_reserved_data= mmap(data1 + 100, 10, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,-1,0);
  assert(some_reserved_data != MAP_FAILED);
  void* data2 = _malloc(100);
  assert(data2);
  assert( (void*)(data1+100) != data2);
  printf("Test #5 passed: heap enlarged\n");
}
int main() {
  test1();
  test2();
  test3();
  test4();
  test5();
}